all: project1
project1: string.o strmain.o
	cc string.o strmain.o -o project1
string.o: string.c mystring.h
	cc -Wall -c string.c
strmain.o: strmain.c string.c mystring.h
	cc -Wall -c strmain.c
clean:
	rm *.o
